﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGenerator))]
public class NoiseDisplayEditor : Editor {

    public override void OnInspectorGUI()
    {
        TerrainGenerator terrainGenerator = (TerrainGenerator)target;

        if (DrawDefaultInspector() == true)
        {
            terrainGenerator.GenerateMainTerrain();
        }

        if(GUILayout.Button("Generate Noise"))
        {
            terrainGenerator.GenerateMainTerrain();
        }
        
    }
}
