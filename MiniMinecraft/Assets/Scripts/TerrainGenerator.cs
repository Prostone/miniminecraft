﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour {
    [Header("Terrain Parameters")]
    public int terrainSize;
    public float noiseScale;
    public int seed;
    public bool seedRandomOnStart = true;
    public Vector2 offset;
    public int maxHeight = 20;
    public BlockType[] blocks;
    private Vector3 playerSpawnPoint;

    [Header("Endless Parameters")]
    public float maxViewDistance = 100;
    public Transform player;
    public float updateTerrainTimerSeconds = 1f;
    public float clearTimerSeconds = 60f;
    public float clearDistance = 100f;

    public static TerrainGenerator instance;
    int chunkSize;
    int chunksVisibleInDistance;
    Dictionary<Vector2, GameObject> terrainChunks = new Dictionary<Vector2, GameObject>();
    List<GameObject> lastVisibleTerrainChunks = new List<GameObject>();

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        if (seedRandomOnStart)
        {
            seed = Random.Range(-10000, 10000);
        }

        GenerateMainTerrain();

        chunkSize = terrainSize;
        chunksVisibleInDistance = Mathf.RoundToInt(maxViewDistance / chunkSize);

        SpawnPlayerOnTop();

        StartCoroutine(UpdateTerrainChunks(updateTerrainTimerSeconds));
        StartCoroutine(ClearDistantChunks(clearTimerSeconds));
    }

    /// <summary>
    /// Create block at specified location, basically called by the player character
    /// </summary>
    public void CreateBlockAtLocation(Vector3 position, GameObject block, Transform parentChunk)
    {
        GameObject spawnedBlock = Instantiate(block, position, Quaternion.identity);
        spawnedBlock.transform.parent = parentChunk;
    }

    /// <summary>
    /// Spawn player at the top of generated terrain
    /// </summary>
    private void SpawnPlayerOnTop()
    {        
        player.position = playerSpawnPoint;
        player.gameObject.SetActive(true);
    }

    /// <summary>
    /// Destroys chunks far away from the player
    /// </summary>
    IEnumerator ClearDistantChunks(float delay)
    {
        while(true)
        {            
            Vector2 playerPosVec2 = new Vector2(player.position.x, player.position.z);
            List<Vector2> keysToRemove = new List<Vector2>();
            foreach (var chunk in terrainChunks)
            {
                if (Vector2.Distance(playerPosVec2, chunk.Key * terrainSize) > clearDistance)
                {                    
                    keysToRemove.Add(chunk.Key);
                }
            }
            foreach (var key in keysToRemove)
            {
                GameObject objToDestroy = terrainChunks[key];
                // Clear distance should be set as a large value so the next lines won't run! But just in case, to prevent null exceptions.
                if (lastVisibleTerrainChunks.Contains(objToDestroy))
                {
                    lastVisibleTerrainChunks.Remove(objToDestroy);
                }
                // Remove from dictionary
                terrainChunks.Remove(key);
                // Finally remove terrain chunk object
                Destroy(objToDestroy);
            }
            yield return new WaitForSeconds(clearTimerSeconds);
            Debug.Log("Cleared " + keysToRemove.Count);
        }
    }

    /// <summary>
    /// Update visible terrain chunks
    /// </summary>
    IEnumerator UpdateTerrainChunks(float delay)
    {
        while (true)
        {
            for (int i = 0; i < lastVisibleTerrainChunks.Count; i++)
            {
                lastVisibleTerrainChunks[i].SetActive(false);
            }
            lastVisibleTerrainChunks.Clear();

            int chunkCoordX = Mathf.RoundToInt(player.position.x / chunkSize);
            int chunkCoordY = Mathf.RoundToInt(player.position.z / chunkSize);

            for (int offsetX = -chunksVisibleInDistance; offsetX <= chunksVisibleInDistance; offsetX++)
            {
                for (int offsetY = -chunksVisibleInDistance; offsetY <= chunksVisibleInDistance; offsetY++)
                {
                    Vector2 currentChunkCoords = new Vector2(chunkCoordX + offsetX, chunkCoordY + offsetY);
                    if (terrainChunks.ContainsKey(currentChunkCoords))
                    {
                        terrainChunks[currentChunkCoords].SetActive(true);
                        lastVisibleTerrainChunks.Add(terrainChunks[currentChunkCoords]);
                    }
                    else
                    {
                        GenerateTerrain(currentChunkCoords);
                        lastVisibleTerrainChunks.Add(terrainChunks[currentChunkCoords]);
                    }
                }
            }
            yield return new WaitForSeconds(delay);
        }

    }

    /// <summary>
    /// Generates starting terrain the player will start on
    /// </summary>
    public void GenerateMainTerrain()
    {
        // Generate noise map
        float[,] noiseMap = PerlinNoise.GenerateNoiseMap(terrainSize, terrainSize, noiseScale, seed, offset);
        int halfSize = (int)Mathf.RoundToInt(terrainSize / 2f);
        // Set player spawn point based on noise map
        playerSpawnPoint = new Vector3(0f, noiseMap[halfSize, halfSize] * maxHeight + 3f, 0f);
        if (!Application.isPlaying)
        {
            // For debugging noisemap in editor
            GetComponent<NoiseDisplay>().DrawNoiseMap(noiseMap);
        }
        else
        {
            // Generate terrain from noisemap
            GenerateTerrainFromNoiseMap(noiseMap, Vector2.zero);
        }
    }

    /// <summary>
    /// Generates terrain with offset in integers (in cartessian coordinates). <0,0> is central chunk, <1,0> is chunk on the right and so on....
    /// </summary>
    private void GenerateTerrain(Vector2 chunkOffset)
    {        
        Vector2 noiseOffset = chunkOffset * terrainSize / noiseScale;
        float[,] noiseMap = PerlinNoise.GenerateNoiseMap(terrainSize, terrainSize, noiseScale, seed, offset + noiseOffset);
        GenerateTerrainFromNoiseMap(noiseMap, chunkOffset);
    }

    private void GenerateTerrainFromNoiseMap(float[,] noiseMap, Vector2 chunkOffset)
    {
        // real offset in world coordinates, so chunk <1,0> * size of the terrain so its moved to the right position
        Vector2 realChunkOffset = chunkOffset * terrainSize;
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);
        // Create new terrain chunk parent object and set its location
        GameObject parentObject = new GameObject("TerrainChunk");
        terrainChunks.Add(chunkOffset, parentObject);
        parentObject.transform.position = new Vector3(realChunkOffset.x, 0f, realChunkOffset.y);
        parentObject.transform.parent = this.transform;       
        // Now create some blocks
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GenerateBlocksColumn(x - width / 2f, y - height / 2f, noiseMap[x, y], parentObject);
            }
        }
        
    }

    // Creates a column of blocks at specified location
    private void GenerateBlocksColumn(float x, float y, float height, GameObject parentObject)
    {
        int currentColumnHeight = Mathf.RoundToInt(height * maxHeight);
        currentColumnHeight = currentColumnHeight <= 0 ? 1 : currentColumnHeight;
        for (int currentHeight = 0; currentHeight < currentColumnHeight; currentHeight++)
        {        
            GameObject blockToSpawn = GetBlockToSpawn((float)currentHeight / (maxHeight - 1));
            Instantiate(blockToSpawn, 
                new Vector3(parentObject.transform.position.x + x, currentHeight, parentObject.transform.position.z + y),
                Quaternion.identity,
                parentObject.transform);     
        }
    }

    /// <summary>
    /// Returns which type of block to spawn based on input height
    /// </summary>
    private GameObject GetBlockToSpawn(float height)
    {
        GameObject blockToSpawn = blocks[0].prefab;
        for (int i = blocks.Length - 1; i >= 0; i--)
        {
            if (blocks[i].maxSpawnHeight >= height)
            {
                blockToSpawn = blocks[i].prefab;
            }
            else
            {
                break;
            }
        }

        return blockToSpawn;
    }


    /// <summary>
    /// Min/Max values for some parameters on pressing Enter
    /// </summary>
    private void OnValidate()
    {
        maxHeight = maxHeight <= 0 ? 1 : maxHeight;
    }

    /// <summary>
    /// Generate chunk info for saving the state of the terrain
    /// </summary>
    public ChunkInfo[] GenerateChunkInfo()
    {
        ChunkInfo[] info = new ChunkInfo[this.transform.childCount];
        for (int i = 0; i < this.transform.childCount; i++)
        {
            Transform currentChunk = transform.GetChild(i);
            info[i].chunkPosition = new Vector3S(currentChunk.position);
            info[i].blockName = new string[currentChunk.childCount];
            info[i].blockPositions = new Vector3S[currentChunk.childCount];
            for (int block = 0; block < currentChunk.childCount; block++)
            {
                Transform currentBlock = currentChunk.GetChild(block);
                info[i].blockName[block] = currentBlock.gameObject.tag;
                info[i].blockPositions[block] = new Vector3S(currentBlock.position);
            }
        }

        return info;
    }

    /// <summary>
    /// Generate terrain based on the provided chunk info array
    /// </summary>
    public void GenerateTerrainFromInfo(ChunkInfo[] info)
    {
        // Destroy any terrain currently in the scene
        for (int i = 0; i < this.transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        // Clear dictionaries and lists with terrain data
        terrainChunks.Clear();
        lastVisibleTerrainChunks.Clear();

        // Create new terrain based on provided positions and block types
        for (int i = 0; i < info.Length; i++)
        {
            // Create new terrain chunk object and set its positions
            GameObject currentChunk = new GameObject("TerrainChunk");
            Vector2 chunkPosDic = new Vector2(info[i].chunkPosition.x / terrainSize, info[i].chunkPosition.z / terrainSize);
            terrainChunks.Add(chunkPosDic, currentChunk);
            currentChunk.transform.position = info[i].chunkPosition.GetVector3();
            currentChunk.transform.parent = this.transform;

            // Create blocks as child of that object
            for (int block = 0; block < info[i].blockName.Length; block++)
            {
                GameObject blockToSpawn = GetBlockFromTag(info[i].blockName[block]);
                Instantiate(blockToSpawn,
                    info[i].blockPositions[block].GetVector3(),
                    Quaternion.identity,
                    currentChunk.transform);
            }
            currentChunk.SetActive(false);
        }
    }

    /// <summary>
    /// Returns a prefab that has the sama name as the saved tag
    /// </summary>
    private GameObject GetBlockFromTag(string tag)
    {
        GameObject block = null;
        for (int i = 0; i < blocks.Length; i++)
        {
            if (tag == blocks[i].name)
            {
                block = blocks[i].prefab;
            }
        }
        return block;
    }

    /// <summary>
    /// Returns struct needed for saving the state of the terrain
    /// </summary>
    public TerrainGeneratorInfo GetGeneratorInfo()
    {
        TerrainGeneratorInfo info = new TerrainGeneratorInfo();
        info.maxHeight = maxHeight;
        info.noiseScale = noiseScale;
        info.offset = new Vector2S(offset);
        info.seed = seed;
        return info;
    }

    /// <summary>
    /// Sets parameters for generating random terrain based on the saved state
    /// </summary>
    public void SetValuesFromInfo(TerrainGeneratorInfo info)
    {
        this.maxHeight = info.maxHeight;
        this.noiseScale = info.noiseScale;
        this.offset = info.offset.GetVector2();
        this.seed = info.seed;
    }
}



[System.Serializable]
public struct BlockType
{
    public string name;
    public GameObject prefab;
    public float maxSpawnHeight;
}
