﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseDisplay : MonoBehaviour {

    public Renderer textureRenderer; // texture for rendering noisemap

    private void Awake()
    {
        textureRenderer.gameObject.SetActive(false);
    }

    /// <summary>
    /// Draws noise map on the texture
    /// </summary>
    public void DrawNoiseMap(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        Texture2D texture = new Texture2D(width, height);
        // Set texture mode so we don't have any antialliasing and wrapping
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        // Create colormap for setting texture at once
        Color[] colorMap = new Color[width * height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                colorMap[y * width + x] = new Color(noiseMap[x,y], noiseMap[x, y], noiseMap[x, y]);
            }
        }
        texture.SetPixels(colorMap);
        texture.Apply();

        // Set the texture
        textureRenderer.sharedMaterial.mainTexture = texture;
        // Scale the texture for better detail visibility
        textureRenderer.transform.localScale = new Vector3(width, 1f, height);
    }
}
