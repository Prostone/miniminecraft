﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerlinNoise {

    /// <summary>
    /// Generates noise map with defined parameters
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="scale"></param>
    /// <param name="seed"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
	public static float[,] GenerateNoiseMap(int width, int height, float scale, int seed, Vector2 offset)
    {
        float[,] noiseMap = new float[width, height];
        // Clamp the scale
        scale = Mathf.Clamp(scale, 0.0001f, int.MaxValue);

        System.Random randomSeedOffset = new System.Random(seed);
        int randomBoundary = 10000;
        float seedOffsetX = randomSeedOffset.Next(-randomBoundary, randomBoundary);
        float seedOffsetY = randomSeedOffset.Next(-randomBoundary, randomBoundary);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float xCoord = (x - width / 2) / scale + seedOffsetX + offset.x ;
                float yCoord = (y - height / 2) / scale + seedOffsetY + offset.y ;
            
                float noiseValue = Mathf.PerlinNoise(xCoord, yCoord);
                noiseMap[x, y] = noiseValue;
            }
        }

        return noiseMap;
    }
}
