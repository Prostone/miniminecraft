﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{

    public float toughness = 1f;
    public float timeToReduceToughness = 1f;
    [HideInInspector]
    public bool isBeingDestroyed = false;
    private float currentTime;
    private bool isDestroyed = false;

    private void Start()
    {
        currentTime = timeToReduceToughness;
    }

    private void Update()
    {
        if (isBeingDestroyed)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
            {
                currentTime = timeToReduceToughness;
                this.toughness -= 1;
            }
        }

        if (this.toughness <= 0 && isDestroyed == false)
        {
            isDestroyed = true;
            Destroy(this.gameObject);
        }
    }

}
