﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour {

    public GameObject pauseScreen;
    public GameObject player;

    string saveFileString = "saveData.dat";
    private bool isPaused = false;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

	public void TogglePause()
    {
        isPaused = !isPaused;
        ToggleCursor(isPaused);
        // Disable player movement
        player.transform.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = !isPaused;
        player.transform.GetComponent<MinecraftPlayer>().enabled = !isPaused;
        // Show/Hide Pause Screen
        pauseScreen.SetActive(isPaused);        
    }

    public void ToggleCursor(bool state)
    {
        Cursor.visible = state;
        if (state == true)
            Cursor.lockState = CursorLockMode.None;
        if (state == false)
            Cursor.lockState = CursorLockMode.Confined;
    }

    /// <summary>
    /// Restarts the level
    /// </summary>
    public void GenerateNew()
    {
        Debug.Log("Restarting");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Save current blocks & player positions
    /// </summary>
    public void SaveState()
    {
        Debug.Log("Saving");
        // Create binary formatter and file stream
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Path.Combine(Application.persistentDataPath, saveFileString), FileMode.Create);
        // Create new game state from objects
        GameState gameState = new GameState();
        gameState.playerPosition = new Vector3S(player.transform.position);
        gameState.chunks = TerrainGenerator.instance.GenerateChunkInfo();
        gameState.terrainInfo = TerrainGenerator.instance.GetGeneratorInfo();
        // Save the current state
        bf.Serialize(file, gameState);

        file.Close();
    }

    /// <summary>
    /// Loads the current game state and sets blocks & player positions
    /// </summary>
    public void LoadState()
    {
        Debug.Log("Loading");
        if (File.Exists(Path.Combine(Application.persistentDataPath, saveFileString)))
        {
            // Create binary formatter and file stream
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath, saveFileString), FileMode.Open);
            // Get game state from binary formatter
            GameState gameState = (GameState)bf.Deserialize(file);
            file.Close();

            // Create terrain based on saved state
            TerrainGenerator.instance.SetValuesFromInfo(gameState.terrainInfo);
            TerrainGenerator.instance.GenerateTerrainFromInfo(gameState.chunks);
            // Set player position
            player.transform.position = gameState.playerPosition.GetVector3();
        }
    }

    public void Quit()
    {
        Debug.Log("Exiting");
        Application.Quit();
    }
}

[System.Serializable]
public struct GameState
{
    public Vector3S playerPosition;
    public ChunkInfo[] chunks;
    public TerrainGeneratorInfo terrainInfo;
}

[System.Serializable]
public struct TerrainGeneratorInfo
{
    public float noiseScale;
    public int seed;
    public Vector2S offset;
    public int maxHeight;
}

[System.Serializable]
public struct ChunkInfo
{
    public Vector3S chunkPosition;
    public string[] blockName;
    public Vector3S[] blockPositions;
}


// Serializable Vector3 and Vector2, don't know why unity doesn't have it serializable already.
[System.Serializable]
public class Vector3S
{
    public float x, y, z;

    public Vector3S(Vector3 v)
    {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
    }

    public Vector3 GetVector3()
    {
        return new Vector3(x, y, z);
    }
}

[System.Serializable]
public class Vector2S
{
    public float x, y;

    public Vector2S(Vector2 v)
    {
        this.x = v.x;
        this.y = v.y;
    }

    public Vector2 GetVector2()
    {
        return new Vector2(x, y);
    }
}