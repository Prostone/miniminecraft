﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinecraftPlayer : MonoBehaviour {

    public Animator handAnimator;
    public bool isBuilding = false;
    public bool isDigging = false;    
    public float raycastLength = 2f;
    public float destroyRaycastLength = 1.2f;
    public LayerMask layerMask;
    public GameObject buildCube;
    public Selection[] selections;
    public int selectedBlock = 0;

    private bool canBuild = false;
    private Vector3 buildPosition;
    private bool buildCubeInCollision;
    private Transform parentBuildChunk;
    private Block currentHitBlock;

	void Update ()
    {
        // Building
        if (isBuilding)
        {
            // Cast a ray to know where to build, and show a transparent build cube there
            RaycastHit hit;
            Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
            Debug.DrawRay(rayOrigin, Camera.main.transform.forward * raycastLength, Color.green);
            // If there is a place to build, show the cube
            if (Physics.Raycast(rayOrigin, Camera.main.transform.forward, out hit, raycastLength, layerMask))
            {
                buildPosition = hit.normal.normalized + hit.transform.position; // normal direction + diameter of cube
                canBuild = true;
                buildCube.SetActive(true);
                buildCube.transform.position = buildPosition;
                parentBuildChunk = hit.transform.parent;
            }
            // Else hide it
            else
            {
                canBuild = false;
                buildCube.SetActive(false);
            }
        }

        // Digging State
        if (isDigging)
        {
            // Cast a ray to know which block player wants to destroy
            RaycastHit hit;
            Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
            Debug.DrawRay(rayOrigin, Camera.main.transform.forward * raycastLength, Color.red);
            if (Physics.Raycast(rayOrigin, Camera.main.transform.forward, out hit, destroyRaycastLength, layerMask))
            {
                if (currentHitBlock == null) { currentHitBlock = hit.transform.gameObject.GetComponent<Block>(); }
                if (currentHitBlock.gameObject != hit.transform.gameObject)
                {
                    StopDestroyingCurrentBlock();
                    currentHitBlock = hit.transform.gameObject.GetComponent<Block>();
                }
            }
            else
            {
                StopDestroyingCurrentBlock();         
                currentHitBlock = null;
             
            }

            // If there is a block to destroy, start destroying it
            if (currentHitBlock != null)
            {
                currentHitBlock.isBeingDestroyed = true;
            }
        }
        else
        {
            StopDestroyingCurrentBlock();
        }

        // What happens on left mouse button press
        if (Input.GetButtonDown("Fire1"))
        {
            // Is player in buidling state?
            if (isBuilding && canBuild)
            {
                // Is there a collision between the player and the build cube?
                Collider[] colliders = Physics.OverlapBox(buildCube.transform.position, buildCube.transform.localScale / 2f, buildCube.transform.localRotation);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject == this.gameObject)
                    {
                        buildCubeInCollision = true;
                        break;
                    }
                }
                // If not we can safely build
                if (buildCubeInCollision == false)
                {
                    // Build Stuff
                    TerrainGenerator.instance.CreateBlockAtLocation(buildCube.transform.position,
                        selections[selectedBlock - 1].prefab,
                        parentBuildChunk);
                }
                // Set to false to be safe
                buildCubeInCollision = false;
            }
            // If not, start digging
            else
            {
                StartDigging();
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (isDigging)
            {
                StopDigging();
            }
        }

        // Building States
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            CheckBlockAndSetState(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            CheckBlockAndSetState(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            CheckBlockAndSetState(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            CheckBlockAndSetState(4);
        }
    }

    void StartDigging()
    {
        isDigging = true;
        handAnimator.SetBool("isDigging", isDigging);        
    }

    void StopDigging()
    {
        isDigging = false;
        handAnimator.SetBool("isDigging", isDigging);
    }

    void StopDestroyingCurrentBlock()
    {
        if (currentHitBlock != null)
        {
            currentHitBlock.isBeingDestroyed = false;
        }
    }

    /// <summary>
    /// Sets the current selected block for building, and calls method for showing it. Sets some boolean values dealing with building as well.
    /// </summary>
    void CheckBlockAndSetState(int pressedBlock)
    {
        if (pressedBlock == selectedBlock)
        {
            selectedBlock = 0;
            isBuilding = false;
            buildCube.SetActive(false);
            handAnimator.SetBool("isBuilding", isBuilding);
            ShowSelection(0);
        }
        else
        {
            selectedBlock = pressedBlock;
            isBuilding = true;
            handAnimator.SetBool("isBuilding", isBuilding);
            ShowSelection(pressedBlock);
        }
    }

    /// <summary>
    /// Shows currently selected block using the overlay UI
    /// </summary>
    void ShowSelection(int selection)
    {
        for (int i = 0; i < selections.Length; i++)
        {
            if (i + 1 == selection)
            {
                selections[i].selection.SetActive(true);
            }
            else
            {
                selections[i].selection.SetActive(false);
            }
        }
    }
}

[System.Serializable]
public struct Selection
{
    public GameObject prefab;
    public GameObject selection;
}
